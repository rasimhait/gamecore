﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class TestLevelObject : MonoBehaviour
{
    public ActionType Method;
    public int        Score;
    public ReactiveCommand<int> WinCommand  = new ReactiveCommand<int>();
    public ReactiveCommand<int> LoseCommand = new ReactiveCommand<int>();



    private void OnMouseDown()
    {
        switch (Method)
        {
            case ActionType.Win:
                WinCommand?.Execute(Score);
                break;

            case ActionType.Lose:
                LoseCommand?.Execute(Score);
                break;
        }
    }


    public enum ActionType
    {
        Win,
        Lose
    }
}
