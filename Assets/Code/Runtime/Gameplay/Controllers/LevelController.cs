﻿using UnityEngine;
using System.Collections;
using Settings;
using System.Collections.Generic;
using UniRx;
using Runtime.Managers.Interfaces;
using System.Linq;
using HelmedStudio.Tools;

namespace Runtime.Gameplay.Controllers
{
    public class LevelController : MonoBehaviour
    {
        [HideInInspector]
        public      int               Id { get; private set; }
        private     IGameManager      gameManager;
        private     TestLevelObject[] testObjects;

        private ReactiveProperty<int> Test = new ReactiveProperty<int>();




        public void Init(IGameManager gameManager, int id)
        {
            this.Id          = id;
            this.gameManager = gameManager;

            FindTestObjects();
            Bind();
        }

        public void LevelStart()
        {
            HelmedLogger.Log("level " + (Id + 1).ToString(), "Started.");
        }

        public void LevelComplete(int score = 0)
        {
            gameManager.UiManager.ScreenGame.Value.DoOnComplete();
            HelmedLogger.Log("level " + (Id + 1).ToString(), "Completed with score: " + score);
        }

        public void LevelFailed(int score = 0)
        {
            gameManager.UiManager.ScreenGame.Value.DoOnFailed();
            HelmedLogger.Log("level " + (Id + 1).ToString(), "Failed with score: " + score);
        }

        private void FindTestObjects()
        {
            testObjects = FindObjectsOfType<TestLevelObject>();
        }

        private void Bind()
        {
            foreach (var obj in testObjects)
            {
                obj.WinCommand
                    .Subscribe(x => LevelComplete(x))
                    .AddTo(obj);

                obj.LoseCommand
                    .Subscribe(x => LevelFailed(x))
                    .AddTo(obj);
            }
        }
    }
}
