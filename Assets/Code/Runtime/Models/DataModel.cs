﻿using UniRx;
using UnityEngine;
using System;
using UnityEngine.Serialization;
using Sirenix.OdinInspector;
using Runtime.Managers.Interfaces;

namespace Runtime.Models
{
    [Serializable]
    public class DataModel
    {
        private IDataManager dataManager;

        public int  LastCompletedLevel = -1;
        public bool RulesAccepted      = false;
        public bool EnableVibro        = true;


        public void Set_LastCompletedLevel(int value) 
        {
            LastCompletedLevel = value;
            dataManager.SaveData();
        }

        public void Set_RulesAccepted(bool value)
        {
            RulesAccepted = value;
            dataManager.SaveData();
        }

        public void Set_VibroState(bool value)
        {
            EnableVibro = value;
            dataManager.SaveData();
        }
        
        public DataModel(IDataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public string ToJson() => JsonUtility.ToJson(this);
        
        public void FromJson(string dataJson)
        {
            var data = JsonUtility.FromJson<DataModel>(dataJson);

            LastCompletedLevel = data.LastCompletedLevel;
            RulesAccepted      = data.RulesAccepted;
            EnableVibro        = data.EnableVibro;
        }
    }
}
