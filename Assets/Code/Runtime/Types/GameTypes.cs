﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runtime.Types
{
    public enum TileType 
    { 
        None,
        Tile,
        Wall,
        Water,
        Hole,
        Finish
    }
    public enum GameLoadType 
    { 
        MenuOnly,
        LevelOnly,
        MenuAndLevel,
    }
}
