﻿using Runtime.Managers.Interfaces;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;

using UnityScenes = UnityEngine.SceneManagement.SceneManager;
using Scene       = UnityEngine.SceneManagement.Scene;
using UnityEditor;

namespace Runtime.Managers
{
    public class SceneManager : MonoBehaviour, ISceneManager
    {
        private Dictionary<int, Scene> LoadedScenes = new Dictionary<int, Scene>();

        public void AddCustomeScene(int sceneID, Scene scene)
        {
            UnityScenes.SetActiveScene(scene);
            LoadedScenes.Add(sceneID, scene);
        }

        /// <summary>
        /// Loads game scene by build index
        /// </summary>
        /// <param name="sceneID"> build index </param>
        /// <param name="onComplete"> do on complete </param>
        public void LoadScene(int sceneID, Action onComplete)
        {
            var loading = UnityScenes.LoadSceneAsync(sceneID, UnityEngine.SceneManagement.LoadSceneMode.Additive);

            loading.completed += (x) => StartCoroutine(FrameAwaitor(_SetActiveScene));

            void _SetActiveScene()
            {
                UnityScenes.SetActiveScene(UnityScenes.GetSceneByBuildIndex(sceneID));
                LoadedScenes.Add(sceneID, UnityScenes.GetActiveScene());
                onComplete?.Invoke();
            }
        }

        /// <summary>
        /// Unload loaded scene by Dictionary key
        /// </summary>
        /// <param name="sceneID"> key </param>
        /// <param name="onComplete"> do on complete </param>
        public void UnloadScene(int sceneID, Action onComplete)
        {
            if (LoadedScenes.ContainsKey(sceneID))
            {
                var unloading = UnityScenes.UnloadSceneAsync(LoadedScenes[sceneID]);
                unloading.completed += (x) => StartCoroutine(FrameAwaitor(() => { LoadedScenes.Remove(sceneID); onComplete.Invoke(); }));
               
            }
        }

        /// <summary>
        /// Helps to await for and of current frame
        /// </summary>
        /// <param name="onComplete"> do on complete </param>
        /// <returns></returns>
        private IEnumerator FrameAwaitor(Action onComplete)
        {
            yield return new WaitForEndOfFrame();
            onComplete?.Invoke();
        }
    }
}
