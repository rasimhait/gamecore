﻿using Runtime.Managers.Interfaces;

public abstract class Manager
{
    public IGameManager GameManager;

    protected Manager(IGameManager gameManager)
    {
        GameManager = gameManager;
    }
}
