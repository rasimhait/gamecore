﻿using HelmedStudio.Tools;
using Runtime.Gameplay.Controllers;
using Runtime.Managers.Interfaces;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.SceneManagement;

namespace Runtime.Managers
{
    public class LevelManager : Manager, ILevelManager
    {
        public IReactiveProperty<LevelController> ActiveLevel { get; private set; } = new ReactiveProperty<LevelController>();
        private int nextLevelID = -1;
        private List<int> targetLevelList = new List<int>();

        public LevelManager(IGameManager gameManager) : base(gameManager)
        {
            nextLevelID = GameManager.DataManager.Data.LastCompletedLevel;
            CheckRemoteLevelList();
        }


        private void CheckRemoteLevelList()
        {
            var remote = GameManager.CoreSettings.RemoteLevelsList;

            if(remote == null || remote == "")
            {
                HelmedLogger.Log("level_manager","No remote levels found. Will use default");
                targetLevelList = GameManager.CoreSettings.LevelScenes;
            }
            else
            {
                var remoteSplitted = remote.Split(',');
                var remoteLevelList = new List<int>();

                foreach (var item in remoteSplitted)
                {
                    var scene = SceneUtility.GetBuildIndexByScenePath("Assets/Content/Levels/" + item + ".unity");
                    remoteLevelList.Add(scene);
                }
                targetLevelList = remoteLevelList;
            }

        }


        public void LoadNext(Action onLoaded = null)
        {
            nextLevelID = (nextLevelID + 1) % targetLevelList.Count;

            if (ActiveLevel.Value == null)
                LoadLevel(nextLevelID, onLoaded);
            else
                ReplaceActiveLevel(nextLevelID, onLoaded);
        }

        private bool FindPreloadedLevel(Action onFound)
        {
            var controllerFound = GameObject.FindObjectOfType<LevelController>();
            if (!controllerFound) return false;

            var scene = controllerFound.gameObject.scene;
            var buildIndex = scene.buildIndex;
            var levelId = targetLevelList.IndexOf(buildIndex);

            controllerFound.Init(GameManager, levelId);
            GameManager.SceneManager.AddCustomeScene(levelId, scene);
            ActiveLevel.Value = controllerFound;

            HelmedLogger.LogWarning("level_manager", "You are on preloaded level, some functions like LoadNext or ReloadLevel will not work!");

            onFound?.Invoke();

            return true;
        }

        /// <summary>
        /// Loads Level
        /// </summary>
        /// <param name="levelID"></param>
        public void LoadLevel(int levelID, Action onLoaded = null)
        {
            if (FindPreloadedLevel(onLoaded))
                return;

            GameManager.SceneManager.LoadScene(targetLevelList[levelID], () =>
            {
                var controller      = UnityEngine.Object.FindObjectOfType<LevelController>();
                var levelSettings   = targetLevelList[levelID];

                controller.Init(GameManager, levelID);
                ActiveLevel.Value = controller;

                onLoaded?.Invoke();
            });
        }


        /// <summary>
        /// Unloads Level
        /// </summary>
        /// <param name="levelID"></param>
        public void UnloadLevel(int levelID, Action onUnloaded = null)
        {
            GameManager.SceneManager.UnloadScene(targetLevelList[levelID], () => {
                ActiveLevel.Value = null;
                onUnloaded?.Invoke();
            });
        }


        public void ReplaceActiveLevel(int levelID, Action onComplete = null)
        {
            UnloadLevel(ActiveLevel.Value.Id, () => LoadLevel(levelID, onComplete));
        }


        public void ReloadActiveLevel(Action onComplete = null)
        {
            var target = ActiveLevel.Value.Id;
            ReplaceActiveLevel(target, onComplete);
        }
    }
}