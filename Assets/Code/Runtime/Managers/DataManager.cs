﻿using Runtime.Managers.Interfaces;
using Runtime.Models;
using UnityEngine;
using UniRx;

namespace Runtime.Managers
{
    public class DataManager : Manager, IDataManager
    {
        public DataModel        Data        { get; private set; }
        public ReactiveCommand  DataLoaded  { get; private set; } = new ReactiveCommand();

        public DataManager(IGameManager gameManager) : base(gameManager)
        {
            Data = new DataModel(this);
            LoadData();
            Bind();
        }

        /// <summary>
        /// Saves active game data
        /// </summary>
        public void SaveData()
        {
            PlayerPrefs.SetString("GAME_PLAYER_DATA", Data.ToJson());
            PlayerPrefs.Save();
            Debug.Log("SAVE " + Data.ToJson());
        }

        /// <summary>
        /// Loads last saved data
        /// </summary>
        public void LoadData()
        {
            if (PlayerPrefs.HasKey("GAME_PLAYER_DATA"))
            {
                var dataString = PlayerPrefs.GetString("GAME_PLAYER_DATA");
                Debug.Log(dataString);
                Data.FromJson(dataString);
                DataLoaded.Execute();
            }
        }


        private void Bind()
        {
            //Data.LastCompletedLevel
            //    .Skip(1)
            //    .Subscribe(x => SaveData())
            //    .AddTo(GameManager.LifeTime);

            //Data.RulesAccepted
            //   .Skip(1)
            //   .Subscribe(x => SaveData())
            //   .AddTo(GameManager.LifeTime);
        }
    }
}
