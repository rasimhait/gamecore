﻿using Runtime.Models;
using UniRx;

namespace Runtime.Managers.Interfaces
{
    public interface IDataManager
    {
        DataModel Data { get;}
        ReactiveCommand DataLoaded { get; }
        void SaveData();
        void LoadData();
    }
}