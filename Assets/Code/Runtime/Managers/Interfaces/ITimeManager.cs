﻿using System;

namespace Runtime.Managers.Interfaces
{
    public interface ITimeManager
    {
        void AwaitFrames(int frames, Action action);
        void AwaitSeconds(int seconds, Action action);
        TimeSpan GetSessionTime();
    }
}