﻿using Runtime.Gameplay.Controllers;
using System;
using UniRx;

namespace Runtime.Managers.Interfaces
{
    public interface ILevelManager
    {
        void LoadNext           (Action onLoaded = null);
        void LoadLevel          (int levelID, Action onLoaded = null);
        void UnloadLevel        (int levelID, Action onUnloaded = null);
        void ReplaceActiveLevel (int levelID, Action onComplete = null);
        void ReloadActiveLevel  (Action onComplete = null);

        IReactiveProperty<LevelController> ActiveLevel { get; }
    }
}