﻿using Runtime.Types;
using Runtime.Tools;
using Settings;
using UniRx;

namespace Runtime.Managers.Interfaces
{
    public interface IGameManager
    {
        SettingsGame   GameSettings { get; }
        SettingsCore   CoreSettings { get; }
        
        IDataManager   DataManager  { get; }
        ILevelManager  LevelManager { get; }
        IUiManager     UiManager    { get; }
        ISceneManager  SceneManager { get; }
        ITimeManager   TimeManager  { get; }
        LifeTime       LifeTime     { get; }
    }
}