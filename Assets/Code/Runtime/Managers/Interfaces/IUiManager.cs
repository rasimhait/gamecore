﻿using UI;
using UniRx;

namespace Runtime.Managers.Interfaces
{
    public interface IUiManager
    {
        IGameManager GameManager { get; }

        ReactiveProperty<UiScreenMenu>   ScreenMenu   { get; set; }
        ReactiveProperty<UiScreenGame>   ScreenGame   { get; set; }
        ReactiveProperty<UiScreenCommon> ScreenCommon { get; set; }
    }
}