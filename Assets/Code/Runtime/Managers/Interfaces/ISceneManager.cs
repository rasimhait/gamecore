﻿using System;
using UnityEngine.SceneManagement;

namespace Runtime.Managers.Interfaces
{
    public interface ISceneManager
    {
        void LoadScene       (int sceneID, Action onComplete);
        void UnloadScene     (int sceneID, Action onComplete);
        void AddCustomeScene (int sceneID, Scene scene);
    }
}