﻿using Runtime.Managers.Interfaces;
using System;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine;

namespace Runtime.Managers
{
    class TimeManager : MonoBehaviour, ITimeManager
    {
        private DateTime sessionStartTime;

        //TODO - Time planed actions 

        private void Awake()
        {
            sessionStartTime = DateTime.Now;
        }

        public TimeSpan GetSessionTime()
        {
            return DateTime.Now - sessionStartTime;
        }

        public void AwaitSeconds(int seconds, Action action)
        {
            StartCoroutine(_AwaitSecondsRoutine(seconds,action));

           
        }
        IEnumerator _AwaitSecondsRoutine(int seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action?.Invoke();
        }

        public void AwaitFrames(int frames, Action action)
        {
            StartCoroutine(_AwaitFramesRoutine());

            IEnumerator _AwaitFramesRoutine()
            {
                int count = 0;

                while (count != frames)
                {
                    yield return new WaitForEndOfFrame();
                    count++;
                }
            }
        }





    }
}
