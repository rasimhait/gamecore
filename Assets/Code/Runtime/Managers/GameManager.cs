﻿using Runtime.Managers.Interfaces;
using Runtime.Tools;
using Runtime.Types;
using Messages;
using Settings;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine.UI;
using System;

namespace Runtime.Managers
{
    public sealed class GameManager : MonoBehaviour,IGameManager
    {
        #region Private

        private DataManager    dataManager;
        private LevelManager   levelManager;
        private UiManager      uiManager;
        private SceneManager   sceneManager;
        private TimeManager    timeManager;

        private LifeTime       lifeTime;

        #endregion

        #region Privat-Serialized

        [LabelText("Settings: Ядро")]
        [SerializeField, InlineEditor(Expanded = true)]
        private SettingsCore coreSettings;

        [LabelText("Setitngs: Игра")]
        [SerializeField, InlineEditor(Expanded = true)]
        private SettingsGame gameSettings;

        #endregion

        public SettingsGame   GameSettings => gameSettings;
        public SettingsCore   CoreSettings => coreSettings;
        public IDataManager   DataManager  => dataManager;
        public ILevelManager  LevelManager => levelManager;
        public IUiManager     UiManager    => uiManager;
        public ISceneManager  SceneManager => sceneManager;
        public ITimeManager   TimeManager  => timeManager;
        public LifeTime       LifeTime     => lifeTime;

        private void Awake()
        {
            AddComponents();
            CreateManagers();       // Create all managers
            Bind();                 // Bind all actions
        }


        void Start()
        {
            OnGameIsReady();
        }


        /// <summary>
        /// Binds all importent actions and ptroperties.
        /// </summary>
        private void Bind()
        {
            //Some binds
        }


        private void OnGameIsReady()
        {
            switch (CoreSettings.GameLoadType)
            {
                case GameLoadType.MenuOnly:
                    UiManager.ScreenMenu.Value.Show();
                    break;

                case GameLoadType.LevelOnly:
                    UiManager.ScreenMenu.Value.Hide();
                    UiManager.ScreenGame.Value.Show();
                    LevelManager.LoadNext(() => LevelManager.ActiveLevel.Value?.LevelStart());
                    break;

                case GameLoadType.MenuAndLevel:
                    UiManager.ScreenMenu.Value.Show();
                    LevelManager.LoadNext();
                    break;

                default:
                    break;
            }
            UiManager.ScreenCommon.Value.Show();
        }

        private void AddComponents()
        {
            timeManager  = gameObject.AddComponent<TimeManager>();
            sceneManager = gameObject.AddComponent<SceneManager>();
            lifeTime     = new GameObject("LifeTime").AddComponent<LifeTime>();
        }


        /// <summary>
        /// Creates all managers.
        /// </summary>
        private void CreateManagers()
        {
            dataManager  = new DataManager(this);
            levelManager = new LevelManager(this);
            uiManager    = new UiManager(this);
        }
    }
}