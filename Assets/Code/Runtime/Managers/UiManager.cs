﻿using Runtime.Managers.Interfaces;
using UniRx;
using UI;
using Messages;
using UnityEngine;
using System;
using Runtime.Gameplay.Controllers;

namespace Runtime.Managers
{
    public class UiManager : Manager, IUiManager
    {
        IGameManager IUiManager.GameManager => GameManager;

        public ReactiveProperty<UiScreenMenu>   ScreenMenu   { get; set; } = new ReactiveProperty<UiScreenMenu>();
        public ReactiveProperty<UiScreenGame>   ScreenGame   { get; set; } = new ReactiveProperty<UiScreenGame>();
        public ReactiveProperty<UiScreenCommon> ScreenCommon { get; set; } = new ReactiveProperty<UiScreenCommon>();


        public UiManager(IGameManager gameManager) : base(gameManager)
        {
            MessageBroker.Default
                .Receive<MessageUiScreenInitRequest>()
                .Subscribe(x => x.Screen.Init(this))
                .AddTo(GameManager.LifeTime);

            ScreenMenu.Subscribe    (x => _ReadyCheck()).AddTo(GameManager.LifeTime);
            ScreenGame.Subscribe    (x => _ReadyCheck()).AddTo(GameManager.LifeTime);
            ScreenCommon.Subscribe  (x => _ReadyCheck()).AddTo(GameManager.LifeTime);

            void _ReadyCheck()
            {
                if (ScreenGame.Value &&
                    ScreenMenu.Value &&
                    ScreenCommon.Value)
                    Ready();
            }
        }

        private void Ready()
        {
            Bind();
        }

        private void Bind()
        {
            GameManager.LevelManager.ActiveLevel
                .Subscribe(x => ScreenCommon.Value.FadeOut())
                .AddTo(GameManager.LifeTime);
        }
    }
}
