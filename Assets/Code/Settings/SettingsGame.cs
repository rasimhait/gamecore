﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Settings
{

    [CreateAssetMenu(fileName ="SettingsGame",menuName ="Game/Settings/Game")]
    public class SettingsGame : SerializedScriptableObject
    {
        [Title("GAME SETTINGS", TitleAlignment = TitleAlignments.Centered, Subtitle = "All configurations of game.")]
        public string YourParam = "Value";


        //TODO : Скрыть после тестов
        //[HideInInspector]

    }
}
