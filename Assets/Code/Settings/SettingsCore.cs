﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Runtime.Types;

namespace Settings
{
    [CreateAssetMenu(fileName = "SettingsCore", menuName = "Game/Settings/Core")]
    public class SettingsCore : SerializedScriptableObject
    {
        [FoldoutGroup("Параметры загрузки")]
        [LabelText("Тип запуска игры:")]
        public GameLoadType GameLoadType;

        [FoldoutGroup("Параметры переходов")]
        [LabelText("Использовать плавные переходы:")]
        public bool UseFade = true;

        [FoldoutGroup("Параметры переходов")]
        [LabelText("Время переходы:"), ShowIf("UseFade")]
        public float FadeDuration;

        [FoldoutGroup("Параметры переходов")]
        [LabelText("Цвет переходов:"), ShowIf("UseFade")]
        public Color FadeColor;

        [FoldoutGroup("Параметры уровней")]
        [InfoBox("Значение перезапишет стандартный список уровней")]
        [LabelText("Строковый список уровней:")]
        public string RemoteLevelsList = null;

        [Space(5)]
        [FoldoutGroup("Параметры уровней")]
        [InfoBox("ID Сцен из <b>BuildSettings</b>")]
        [ListDrawerSettings(Expanded = false, ShowIndexLabels = true)]
        [LabelText("Level IDs:")]
        public List<int> LevelScenes = new List<int>();
    }
}
