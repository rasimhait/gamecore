﻿using UI.Interfaces;

namespace Messages
{
    public class MessageUiScreenInitRequest
    {
        public IUiScreen Screen;

        public MessageUiScreenInitRequest(IUiScreen screen)
        {
            Screen = screen;
        }
    }
}
