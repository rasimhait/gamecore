﻿using Runtime.Managers.Interfaces;
using UnityEngine;

namespace UI.Interfaces
{
    public interface IUiScreen
    {
        IUiManager UiManager {get;}
        void Init(IUiManager uiManager);
        void Show();
        void Hide();
    }
}
