﻿using Messages;
using Runtime.Managers;
using Runtime.Managers.Interfaces;
using Sirenix.OdinInspector;
using UI.Interfaces;
using UniRx;
using UnityEngine;

namespace UI
{
    public abstract class UiScreen : SerializedMonoBehaviour, IUiScreen
    {
        public IUiManager UiManager { get; private set; }
        public GameObject Content;


        public void Init(IUiManager uiManager)
        {
            UiManager = uiManager;
            OnInitialized();
        }


        public virtual void Show()
        {
            Content.SetActive(true);
        }

        public virtual void Hide()
        {
            Content.SetActive(false);
        }


        private void Start()
        {
            MessageBroker.Default.Publish(new MessageUiScreenInitRequest(this));
            Hide();
        }


        public abstract void OnInitialized();
    }
}
