﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UI
{
    public class UiScreenGame : UiScreen
    {
        public Button UiButton_NextLevelButton;
        public Button UiButton_RetryButton;

        public TMP_Text LevelLabel;

        public GameObject FaildeScreen;
        public GameObject CompletedScreen;

        public override void OnInitialized()
        {
            UiManager.ScreenGame.Value = this;
            Bind();
        }

        private void Bind()
        {
            UiButton_NextLevelButton.OnClickAsObservable()
                .Subscribe  (x => OnNextLevelButton())
                .AddTo      (this);

            UiButton_RetryButton.OnClickAsObservable()
                .Subscribe  (x => OnRetyLevelButton())
                .AddTo      (this);

            UiManager.GameManager.LevelManager.ActiveLevel                                                 //Вызывается при каждом зменении реактивного значения ActiveLevel
                .Where      (x => x != null)                                                               //Если текущее значение не NULL
                .Do         (x => LevelLabel.text = "LEVEL "+ (x.Id + 1).ToString())                       //Изменяет подпись шапки на Level {ID}
                .Subscribe  (x => { FaildeScreen.SetActive(false); CompletedScreen.SetActive(false); })    //Выключает окна поражения и победы если уровень изменился
                .AddTo      (this);                                                                        //Привязывает данную подписку на себя (если объект удалится или выключится, подписка тоже пропадет)
        }

        public void DoOnComplete()
        {
            CompletedScreen.SetActive(true);
        }

        public void DoOnFailed()
        {
            FaildeScreen.SetActive(true);
        }

        private void OnRetyLevelButton()
        {
            if (UiManager.GameManager.CoreSettings.UseFade)
                UiManager.ScreenCommon.Value.FadeIn(() => UiManager.GameManager.LevelManager.ReloadActiveLevel(() => UiManager.GameManager.LevelManager.ActiveLevel.Value.LevelStart()));
            else
                UiManager.GameManager.LevelManager.ReloadActiveLevel(() => UiManager.GameManager.LevelManager.ActiveLevel.Value.LevelStart());
        }

        private void OnNextLevelButton()
        {
            if (UiManager.GameManager.CoreSettings.UseFade)
                UiManager.ScreenCommon.Value.FadeIn(() => UiManager.GameManager.LevelManager.LoadNext(() => UiManager.GameManager.LevelManager.ActiveLevel.Value.LevelStart()));
            else
                UiManager.GameManager.LevelManager.LoadNext(() => UiManager.GameManager.LevelManager.ActiveLevel.Value.LevelStart());

        }
    }
}
