﻿using UniRx;
using UnityEngine.UI;

namespace UI
{
    public class UiScreenMenu : UiScreen
    {
        public Button Button_Play;

        public override void OnInitialized()
        {
            UiManager.ScreenMenu.Value = this;

            Bind();
        }

        private void Bind()
        {
            Button_Play.onClick.AsObservable()
                .Subscribe(x => OnPlayButtonPressed())
                .AddTo(this);
        }


        private void OnPlayButtonPressed()
        {
            if (UiManager.GameManager.LevelManager.ActiveLevel.Value == null)
            {
                UiManager.ScreenCommon.Value.FadeIn(() =>
                {
                    Hide();
                    UiManager.GameManager.LevelManager.LoadNext(() => UiManager.GameManager.LevelManager.ActiveLevel.Value.LevelStart());
                    UiManager.ScreenGame.Value.Show();
                });
            }
            else
            {
                Hide();
                UiManager.ScreenGame.Value.Show();
            }
        }
    }
}
