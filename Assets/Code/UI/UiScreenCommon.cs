﻿using GGTools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UiScreenCommon : UiScreen
    {
        private Color     fadeInTargetColor  = new Color(1, 1, 1, 1);
        private Color     fadeOutTargetColor = new Color(1, 1, 1, 0);
        private Coroutine fading;
        private int       tweenId;


        public override void OnInitialized()
        {
            UiManager.ScreenCommon.Value = this;
            fadeInTargetColor = UiManager.GameManager.CoreSettings.FadeColor;
            fadeOutTargetColor = new Color(fadeInTargetColor.r, fadeInTargetColor.g, fadeInTargetColor.b, 0);

        }

        public Image UiElement_Fade;


        private void Awake()
        {
            UiElement_Fade.color = fadeOutTargetColor;
        }

        public void Fade(float waitTime, Action onFadeInComplete, Action onFadeOutComplete)
        {
            if (fading == null) fading = StartCoroutine(FadeRoutine(waitTime, onFadeInComplete, onFadeOutComplete));
        }

        public void FadeIn(Action onComplete = null)
        {
            if(!UiManager.GameManager.CoreSettings.UseFade)
            {
                onComplete?.Invoke();
                return;
            }

            TweenModule.EndTween(tweenId);

            tweenId = TweenModule.TweenColor(
                (x) => UiElement_Fade.color = x,
                UiElement_Fade.color,
                fadeInTargetColor,
                UiManager.GameManager.CoreSettings.FadeDuration,
                callback: onComplete);
        }

        public void FadeOut(Action onComplete = null)
        {
            if (!UiManager.GameManager.CoreSettings.UseFade)
            {
                onComplete?.Invoke();
                return;
            }
            TweenModule.EndTween(tweenId);

            tweenId = TweenModule.TweenColor(
                (x) => UiElement_Fade.color = x,
                UiElement_Fade.color,
                fadeOutTargetColor,
                UiManager.GameManager.CoreSettings.FadeDuration,
                callback: onComplete);
        }

        private IEnumerator FadeRoutine(float waitTime, Action onFadeInComplete, Action onFadeOutComplete)
        {
            FadeIn(onFadeInComplete);
            yield return new WaitForSeconds(waitTime);
            FadeOut(onFadeOutComplete);
        }
    }
}
