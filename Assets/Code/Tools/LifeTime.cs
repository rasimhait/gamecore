﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Runtime.Tools
{
    public sealed class LifeTime : MonoBehaviour
    {
        void Awake() => DontDestroyOnLoad(gameObject);
    }
}
