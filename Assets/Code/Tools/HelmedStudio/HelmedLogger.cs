﻿using UnityEngine;

namespace HelmedStudio.Tools
{
    public static class HelmedLogger
    {

        public static void Log(string tag, string message)
        {
            Debug.Log("<color=green><b>" + tag.ToUpper() + ": </b></color>" + message);
        }

        public static void LogWarning(string tag, string message)
        {
            Debug.Log("<color=yellow><b>" + tag.ToUpper() + ": </b></color>" + message);
        }

        public static void LogError(string tag, string message)
        {
            Debug.Log("<color=red><b>" + tag.ToUpper() + ": </b></color>" + message);
        }

    }
}